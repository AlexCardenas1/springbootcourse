package com.bolsadeideas.springboot.web.app.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Developer
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Usuario {

	private String nombre;
	private String apellido;
	private String email;

}
