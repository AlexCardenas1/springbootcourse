package com.bolsadeideas.springboot.web.app.controllers;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bolsadeideas.springboot.web.app.models.Usuario;

/**
 * Controlador principal de la aplicacion Encargado de manejar las peticiones
 * del usuario
 * 
 * @author Developer
 *
 */
@Controller
@RequestMapping("/app") // Ruta generica de acceso, ruta de primer nivel
public class IndexContoller {

	@RequestMapping(value = "/index", method = RequestMethod.GET) // Esta es una forma
	public String index() {

		return "index";
	}

	// Este método lo vamos a mapear a varias rutas, se hace con llaves separando
	// con comas
	@GetMapping({ "/indexGet", "/", "/home", "" }) // Esta es otra forma para las peticiones, hace lo mismo que le
													// metodo anterior
	public String indexGet(Model model, ModelMap modelMap) {
		model.addAttribute("titulo", "Hola Spring Framework!"); // Esta es una forma de hacerlo
		modelMap.addAttribute("tituloMap", "HolaSpringFrameworkMap!");
		return "index";
	}

	@RequestMapping("/perfil")
	public String perfil(Model model) {
		Usuario usuario = new Usuario();
		usuario.setNombre("Alexander");
		usuario.setApellido("Cardenas");
		usuario.setEmail("alexcardenas.u@gmail.com");
		model.addAttribute("titulo", "Perfil del usuario ".concat(usuario.getNombre()));
		model.addAttribute("usuario", usuario);
		return "perfil";
	}

	@RequestMapping("/listar")
	public String listar(Model model) {
		model.addAttribute("titulo", "Listado de usuarios ");
		return "listar";
	}
	
	//Este metodo es en común para todos los métodos del controlador
	//Es decir el objeto usuario se va a retornar a todos los métodos handler
	@ModelAttribute("usuarios")
	public List<Usuario> poblarUsuarios(){
		List<Usuario> usuarios = Arrays.asList(new Usuario("Pedro", "Cardenas", "alexcardenas@correo.com"),
				new Usuario("Camila", "Fernandez", "cfernandez@correo.com"),
				new Usuario("Enrique", "Granados", "egranados@correo.com"),
				new Usuario("Felipe", "Mendieta", "fmendieta@correo.com"));
		return usuarios;
	}
}
